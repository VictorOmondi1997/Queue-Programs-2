#include <iostream>
#include <conio.h>

using namespace std;

class queue{
public:
    int q[5],front,rear,x,result;
    void enq();
    void dque();
    void disp();

    queue(){
        front =0;
        rear=0;
    }
};

void queue::enq(){
    if(rear>5)
        cout<< "\nQueue Overflow!!\n";
    else{
        cout<< "\nEnter the number to be inserted: ";
        cin>> x;
        rear++;
        q[rear]=x;
        cout<< "\nNumber Pushed In The Queue: "<<q[rear];
    }
}
void queue::dque(){
    if(rear==0)
        cout<< "\nQueue UnderFlow!!\n";
    else{
        if(front==rear){
            front=0;
            rear=0;
        }
        else
            front++;
    }
    cout<< "\nDeleted Element is: ";
    result=q[front];
    cout<<result;
}
void queue::disp(){
    if(rear==0)
        cout<< "\nQueue Underflow!!\n";
    else
        cout<< "\nContents of Queue is: ";
    for(int i=front+1;i<=rear;i++)
        cout<< q[i]<< "\t";
}

int main()
{
    int c;
    queue qu;
    //clrscr();
    cout<< "\n\t***********";
    cout<< "\n\t*Q U E U E*";
    cout<< "\n\t***********";

    do{
        cout<< "\n\t1.Insertion \n\t2.Deletion \n\t3.Display\n";
        cout<< "\n\tEnter Your Choice: ";
        cin>> c;
        switch(c){
        case 1:
            qu.enq();
            break;
        case 2:
            qu.dque();
            break;
        case 3:
            qu.disp();
            break;
        default:
            cout<< "\nInvalid Choice!!\n";
        }
    }
    while(c<4);
    getch();
}
